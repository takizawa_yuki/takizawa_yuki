<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ page import="takizawa_yuki.beans.UserMessage" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム</title>
		<link href="./css/style.css" rel="stylesheet">
		<script type="text/javascript" src="./js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="./js/jquery.menu.min.js"></script>
		<script defer src="./js/fontawesome-all.min.js"></script>
	</head>
	<body>




		<div class="main-contents">
			<div class="border">
				<div class="header">
					<a href="messages" class="menu">新規投稿</a>
					<c:if test="${loginUser.departmentId == 1 }">
						<a href="management" class="menu">ユーザー管理</a>
					</c:if>
					<a href="https://zxcvbnmnbvcxz.com/" class="menu" id="menu">検索</a>
					<a href="logout" class="logout">ログアウト</a>
				</div>

				<c:if test="${sliderefine == null }">
					<div class="refine">
						<div class="rebuttom"><form action="./" accept-charset="UTF-8">
							<input class="rebuttom" type="submit" value="検索リセット" />
						</form></div>
						<form action="./" accept-charset="UTF-8">
							<input class="categorytext" name="category" value="${category }" id="category" placeholder="カテゴリ" />

							&emsp;<input class="refinedata" type="date" name="date1" value="${createdDateMin }" id="date1" />
							 ～ <input class="refinedata" type="date" name="date2" value="${createdDateMax }" id="date2" />
							<input class="form" type="submit" value="検索" />
						</form>
					</div>
				</c:if>

				<c:if test="${sliderefine != null }">
					<div class="refine2">
						<div class="rebuttom"><form action="./" accept-charset="UTF-8">
							<input class="rebuttom" type="submit" value="検索リセット" />
						</form></div>
						<form action="./" accept-charset="UTF-8">
							<input class="categorytext" name="category" value="${category }" id="category" placeholder="カテゴリ" />

							&emsp;<input class="refinedata" type="date" name="date1" value="${createdDateMin }" id="date1" />
							 ～ <input class="refinedata" type="date" name="date2" value="${createdDateMax }" id="date2" />
							<input class="form" type="submit" value="検索" />
						</form>
					</div>
				</c:if>

				<c:if test="${ not empty errorMessages }">
					<br />
					<div class="errorMessages">
						<ul>
						<c:forEach items="${ errorMessages }" var="errorMessages">
							<li><c:out value="${errorMessages }" />
						</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if>


				<br /><br />

				<c:if test="${empty messages }">
					<br />
					<div class="empty"><c:out value="投稿がありません" /></div><br /><br /><br />
				</c:if>


				<c:forEach items="${messages}" var="messages">
					<div  class="messages">
						<div class="category"><c:out value="カテゴリ：${messages.category }"/></div>
						<div  class="name"><span><c:out value="投稿者：${messages.name}" /></span></div><br />
						<div class="subject"><c:out value="${messages.subject}" /></div>
						<div class="text"><br />
							<c:forEach var="message"  items="${ fn:split(messages.text ,'
') }" >
								<c:out value="${message}" /><br/><br/>
							</c:forEach>
						</div>
						<c:if test="${messages.userId == loginUser.id || loginUser.departmentId == 2}">
							<form action="./" method="post">
								<input name="id" type="hidden" value="${ messages.id }" />
								<span class="delete">
								<button type="submit" class="deleteform" onClick="return confirm('投稿を削除しますか？')">
								<i class="fas fa-trash-alt"></i>
								</button>
								</span>
							</form>
						</c:if>

						<div class="createdDate"><span><fmt:formatDate value="${messages.createdDate }"
						pattern="yyyy/MM/dd HH:mm:ss" /></span></div>

					</div><br />
					<c:forEach items="${comments }" var="comments">
						<c:if test="${messages.id == comments.messageId}">
							<div class="comments">
								<div class="nameC"><c:out value="${comments.name}" /></div>
								<div class="text">
									<c:forEach var="comment"  items="${ fn:split(comments.text ,'
') }" >									<c:out value="${comment}" /><br/>
									</c:forEach>
								</div>

								<c:if test="${comments.userId == loginUser.id || loginUser.departmentId == 2}">
									<form action="./" method="post">
										<input name="id" type="hidden" value="${ comments.id }" />
										<span class="delete">
										<button type="submit" class="deleteform" onClick="return confirm('投稿を削除しますか？')">
										<i class="fas fa-trash-alt"></i>
										</button>
										</span>
									</form>
								</c:if>

								<div class="createdDate">
								<span><fmt:formatDate value="${comments.createdDate }"
								pattern="yyyy/MM/dd HH:mm:ss"/></span>
								</div>

							</div><br /><br />
						</c:if>
					</c:forEach><br />

						<div class="comment"><form action="./" method="post">
							コメント（500文字以内）<br />
							<input name="messageId" value="${messages.id}" type="hidden" />
							<textarea class="text"  name="text" cols="50" rows="8"></textarea><br /><br />
							<div class="buttom"><input class="form" type="submit" value="投稿"></div>
						</form>
						</div><br />
				</c:forEach>
				<p id="pageTop"><a href="">top</a></p>

				<div class="copyright">Copyright(c)Yuki Takizawa</div>
			</div>
		</div>
	</body>
</html>