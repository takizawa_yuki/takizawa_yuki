<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
		<link href="./css/style.css" rel="stylesheet">
	</head>
	<body>
		<div class="main-contents">
			<div class="border">
				<div class="header">
					<a href="./" class="menu">ホーム</a><br />
				</div><br />
				<div class="pagetitle"><c:out value="新規投稿" /></div>


				<c:if test="${not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages }" var="messages">
								<li><c:out value="${messages }" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if><br />
				<div class="newmessage">
					<form action="messages" method="post">
						<label for="subject">件名（30文字以内）</label><br />
						<input type="text" class="textarea" name="subject" value="${message.subject }"
						id="subject" /><br /><br />

						本文（1000文字以内）<br />
						<textarea class="text" name="text" cols="50" rows="10" >${message.text}</textarea><br /><br />

						<label for="category">カテゴリー（10文字以内）</label><br />
						<input type="text" class="textarea" name="category" value="${message.category }"
						id="category"><br /><br />

						<div class="buttom"><input class="form" type="submit" value="投稿" ></div><br />
					</form>
				</div>
				<div class="copyright">Copyright(c)Yuki Takizawa</div>
			</div>
		</div>
	</body>
</html>