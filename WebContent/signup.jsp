<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー登録</title>
		<link href="./css/style.css" rel="stylesheet">
	</head>
	<body>
		<div class="main-contents">
			<div class="border">
				<div class="header">
					<a href="management" class="menu">ユーザー管理</a><br />
				</div><br />
				<div class="pagetitle"><c:out value="ユーザー登録" /></div>
				<c:if test="${not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages }" var="messages">
								<li><c:out value="${messages }" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" />
				</c:if><br />

				<div class="signup">
					<form  action="signup" method="post">
						<label for="name">名前（10文字以下）</label><br />
						<input  type="text" class="stext" name="name" value="${signupUser.name }" id="name" /><br /><br />

						<label for="loginId">ログインID（6文字以上20文字以下）</label><br />
						<input type="text" name="loginId" value="${signupUser.loginId }" id="loginId"><br /><br />

						<label for="password">パスワード（6文字以上20文字以下）</label><br />
						<input class="password" name="password" type="password" id="password"><br /><br />

						<label for="password2">パスワード確認用</label><br />
						<input class="password" name="password2" type="password" id="password2"><br /><br />

						<label for="branchId">支店</label><br />
							<select name="branchId">
								<option value="">選択してください</option>
								<c:forEach items="${branches }" var="branches">
									<option value="${ branches.id}"
									<c:if test="${branches.id == signupUser.branchId }">selected</c:if>>
									${branches.branchName }</option>
								</c:forEach>
							</select><br /><br />


						<label for="departmentId">役職・部署</label><br />
							<select name="departmentId">
								<option value="">選択してください</option>
								<c:forEach items="${departments }" var="departments">
									<option value="${ departments.id}"
									<c:if test="${departments.id == signupUser.departmentId }">selected</c:if>>
									${departments.departmentName }</option>
								</c:forEach>
							</select><br /><br />


						<div class="buttom"><input class="form" type="submit" value="登録" ><br /></div>

					</form>
				</div><br />
				<div class="copyright">Copyright(c)Yuki Takizawa</div>
			</div>
		</div>
	</body>
</html>