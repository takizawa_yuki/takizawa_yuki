<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ログイン</title>
		<link href="./css/style.css" rel="stylesheet">
	</head>
	<body>

		<div class="main-contents">
		<div class="border"><br />
			<div class="title"><c:out value="掲示板課題" /></div>
			<c:if test="${ not empty errorMessages }">
			<br /><br />
				<div class="errorMessages">
					<ul>
					<c:forEach items="${ errorMessages }" var="messages">
						<li><c:out value="${messages }" />
					</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<div class="login">
				<form action="login" method="post">
					<br />
					<label for="loginId">ログインＩＤ</label><br />
					<input class="logintext" name="loginId" value="${retention }" id="loginId"><br /><br />

					<label for="password">パスワード</label><br />
					<input class="logintext" name="password" type="password" id="password" /><br /><br />

					<div class="loginbuttom"><input class="loginbuttom" type="submit" value="ログイン"></div><br />
				</form>
			</div><br />
			<div class="copyright">Copyright(c)Yuki Takizawa</div>
		</div></div>

	</body>
</html>