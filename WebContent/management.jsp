<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理</title>
		<link href="./css/style.css" rel="stylesheet">
	</head>
	<body>
		<div class="main-contents">
			<div class="border">
				<div class="header">
					<a href="./" class="menu">ホーム</a>
					<a href="signup" class="menu">ユーザー登録</a><br />
				</div><br />
				<div class="pagetitle"><c:out value="ユーザー管理"/></div><br />

				<c:if test="${not empty errorMessages }">
					<div class="errorMessages">
						<ul>
							<c:forEach items="${errorMessages }" var="messages">
								<li><c:out value="${messages }" />
							</c:forEach>
						</ul>
					</div>
					<c:remove var="errorMessages" scope="session" /><br />
				</c:if>


				<table id="table" >
					<tr>
						<th>名前</th>
						<th>ログインID</th>
						<th>支店</th>
						<th>部署、役職</th>
						<th>アカウント</th>

					</tr>

					<c:forEach items="${users}" var="users">
						<tr>
							<td>${users.name}</td>
							<td><a href="settings?id=${users.id }">${users.loginId}</a></td>
							<td>${users.branchName}</td>
							<td>${users.departmentName}</td>

							<c:if test="${loginUser.id == users.id }">
							<td>ログイン中
							</td>
							</c:if>

							<c:if test="${loginUser.id != users.id }">
								<td>
								<c:if test="${users.isDeleted == 0 }">
									<form class="stop" action="management" method="post">
										<input name="isDeleted" value="1" id="isDeleted" type="hidden">
										<input name="id" value="${users.id }" id="id"  type="hidden">
										<input class="form" id="stop" type="submit" value="停止" onClick="return confirm('アカウントを停止しますか？')">
										</form>
								</c:if>
								<c:if test="${users.isDeleted == 1 }">
								<form class="recovery" action="management" method="post" >
									<input name="isDeleted" value="0" id="isDeleted"  type="hidden">
									<input name="id" value="${users.id }" id="id"  type="hidden">
									<input class="form" id="recovery" type="submit" value="復活" onClick="return confirm('アカウントを復活しますか？')">
								</form>
								</c:if>
								</td>
							</c:if>

						</tr>
					</c:forEach>
				</table>
				<br/>
				<div class="copyright">Copyright(c)Yuki Takizawa</div>
			</div>
		</div>

	</body>
</html>