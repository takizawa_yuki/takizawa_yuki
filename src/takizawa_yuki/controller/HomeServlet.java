package takizawa_yuki.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import takizawa_yuki.beans.Comments;
import takizawa_yuki.beans.User;
import takizawa_yuki.beans.UserComment;
import takizawa_yuki.beans.UserMessage;
import takizawa_yuki.service.CommentService;
import takizawa_yuki.service.DeleteService;
import takizawa_yuki.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request , HttpServletResponse response)
				throws IOException , ServletException{

		List<UserMessage> messages = new MessageService().getMessage();
		List<UserComment> comments = new CommentService().getComment();

		request.setAttribute("sliderefine", request.getParameter("category"));

		if(request.getParameter("date1") != null && request.getParameter("date2") != null){
			List<UserMessage> refine = refine(request);
			request.setAttribute("messages", refine);
			request.setAttribute("comments", comments);
		}else{
			request.setAttribute("messages", messages);
			request.setAttribute("comments", comments);
		}

		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request , HttpServletResponse response)
				throws IOException , ServletException{

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		List<String> errorMessages = new ArrayList<String>();

		if(request.getParameter("id") != null){
			delete(request);
			response.sendRedirect("./");
			return;
		}

		if(isValid(request , errorMessages) == true){
			Comments comment = new Comments();
			comment.setText(request.getParameter("text"));
			comment.setUserId(user.getId());
			comment.setMessageId(Integer.parseInt(request.getParameter("messageId")));

			new CommentService().register(comment);
			response.sendRedirect("./");
		}else{
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");

		}
	}

	private boolean isValid(HttpServletRequest request , List<String> errorMessages){
		String text = request.getParameter("text");
		HttpSession session = request.getSession();

		if(StringUtils.isBlank(text)){
			errorMessages.add("コメントを入力してください");
			session.setAttribute("errorMessages", errorMessages);
		}
		if(text.length() > 500){
			errorMessages.add("コメントは500文字以内で入力してください");
			session.setAttribute("errorMessages", errorMessages);
		}

		if(errorMessages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
	private void delete(HttpServletRequest request){
		int id = Integer.parseInt(request.getParameter("id"));
		new DeleteService().messageDelete(id);
		new DeleteService().commentDelete(id);

	}

	private List<UserMessage> refine(HttpServletRequest request){
		String category = request.getParameter("category");
		Date createdDateMin = null;
		Date createdDateMax = null;

		if(request.getParameter("date1") == ""){
			createdDateMin = MessageService.createdDateMin().getCreatedDate();


		}else{
			createdDateMin = Date.valueOf(request.getParameter("date1"));
			request.setAttribute("createdDateMin", createdDateMin);

		}
		if(request.getParameter("date2") == ""){
			createdDateMax = MessageService.createdDateMax().getCreatedDate();

		}else{
			createdDateMax = Date.valueOf(request.getParameter("date2"));
			request.setAttribute("createdDateMax", createdDateMax);

		}

		List<UserMessage> refine = new MessageService().getMessage
				(request.getParameter("category") , createdDateMin , createdDateMax);
		request.setAttribute("category", category);

		return refine;
	}
}
