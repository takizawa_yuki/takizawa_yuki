package takizawa_yuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import takizawa_yuki.beans.BranchesDepartments;
import takizawa_yuki.beans.User;
import takizawa_yuki.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request , HttpServletResponse response)
				throws ServletException , IOException{
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		List<BranchesDepartments> branches = new UserService().getBranch();
		List<BranchesDepartments> departments = new UserService().getDepartment();

		if(urlIsValid(request , messages) == false ){
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
			return;
		}


		User editUser =new UserService().getUser(Integer.parseInt(request.getParameter("id")));
		request.setAttribute("editUser", editUser);
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);
		session.getAttribute("loginUser");



		request.getRequestDispatcher("settings.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request , HttpServletResponse response)
				throws ServletException , IOException{

		List<String> messages = new ArrayList<String>();
		String password = request.getParameter("password");


		if(isValid(request , messages) == true){
			if(password == ""){
				User emptyPassEditUser = (User) new UserService().getUser(Integer.parseInt(request.getParameter("id")));
				User editUser = getEditUser(request , emptyPassEditUser.getPassword());
				new UserService().emptyPassUpdate(editUser);

				response.sendRedirect("management");
			}else{
				User editUser = getEditUser(request , password);
				request.setAttribute("editUser", editUser);

				new UserService().update(editUser);

				response.sendRedirect("management");
			}
		}else{

			request.setAttribute("errorMessages", messages);
			List<BranchesDepartments> branches = new UserService().getBranch();
			List<BranchesDepartments> departments = new UserService().getDepartment();
			User editUser = getEditUser(request , password);

			request.setAttribute("branches", branches);
			request.setAttribute("departments", departments);
			request.setAttribute("editUser", editUser);

			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser (HttpServletRequest request , String password)throws IOException , ServletException{
		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setPassword(password);
		editUser.setName(request.getParameter("name"));
		if(request.getParameter("branchId") != ""){
			editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		}
		if(request.getParameter("departmentId") != ""){
			editUser.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		}

		return editUser;

	}
	private boolean isValid(HttpServletRequest request , List<String> messages){
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));

		if(StringUtils.isBlank(name)){
			messages.add("名前を入力してください");
			request.setAttribute("errorMessages", messages);

		}
		if(name.length() > 10){
			messages.add("名前は10文字以下で入力してください");
			request.setAttribute("errorMessages", messages);

		}
		if(loginId == ""){
			messages.add("ログインIDを入力してください");
			request.setAttribute("errorMessages", messages);

		}else if(loginId.length() < 6 || loginId.length() > 20){
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
			request.setAttribute("errorMessages", messages);

		}else if(!request.getParameter("loginId").matches("^[0-9a-zA-Z]+$")){
			messages.add("ログインIDは半角英数字で入力してください");
			request.setAttribute("errorMessages", messages);

		}
		if(password != "" || password2 != ""){
			if(password.length() < 6 || password.length() > 20){
				messages.add("パスワードは6文字以上20文字以下で入力してください");
				request.setAttribute("errorMessages", messages);

			}else if(!request.getParameter("password").matches("^[0-9a-zA-Z]+$")){
				messages.add("パスワードは半角英数字で入力してください");
				request.setAttribute("errorMessages", messages);

			}else if(!password.equals(password2)){
				messages.add("パスワードが一致しません");
				request.setAttribute("errorMessages", messages);

			}
		}
		if(!request.getParameter("loginId").equals(editUser.getLoginId())){
			if(new UserService().getLoginId(request.getParameter("loginId")).getLoginId() != null){
				messages.add("ログインIDが重複しています");
				request.setAttribute("errorMessages", messages);

			}
		}
		if(request.getParameter("branchId") == ""){
			messages.add("支店を選択してください");
			request.setAttribute("errorMessages", messages);

		}
		if(request.getParameter("departmentId") == ""){
			messages.add("役職を選択してください");
			request.setAttribute("errorMessages", messages);

		}
		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}

	private boolean urlIsValid(HttpServletRequest request , List<String> messages){
		HttpSession session = request.getSession();
		if(request.getParameter("id") == null){
			messages.add("不正なパラメータです");
			session.setAttribute("errorMessages", messages);
		}else if(!request.getParameter("id").matches("^[0-9]+$")){
			messages.add("不正なパラメータです");
			session.setAttribute("errorMessages", messages);

		}else if(new UserService().getUser(Integer.parseInt(request.getParameter("id"))) == null){
			messages.add("不正なパラメータです");
			session.setAttribute("errorMessages", messages);
		}
		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
