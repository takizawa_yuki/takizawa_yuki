package takizawa_yuki.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import takizawa_yuki.beans.User;
import takizawa_yuki.service.UserService;

@WebServlet(urlPatterns = {"/management"})
public class ManagementServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request , HttpServletResponse response)
				throws IOException , ServletException{

		HttpSession session = request.getSession();
		session.getAttribute("loginUser");

		List<User> users = new UserService().getUser();

		request.setAttribute("users", users);

		request.getRequestDispatcher("/management.jsp").forward(request, response);
	}
	@Override
	protected void doPost(HttpServletRequest request , HttpServletResponse response)
				throws ServletException , IOException{


		User user = new User();
		user.setId(Integer.parseInt(request.getParameter("id")));
		user.setIsDeleted(Integer.parseInt(request.getParameter("isDeleted")));

		new UserService().isDeleted(user);

		response.sendRedirect("management");
	}
}

