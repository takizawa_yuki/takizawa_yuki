package takizawa_yuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import takizawa_yuki.beans.Message;
import takizawa_yuki.beans.User;
import takizawa_yuki.service.MessageService;

@WebServlet(urlPatterns = { "/messages" })
public class NewMessages extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet (HttpServletRequest request , HttpServletResponse response)
				throws IOException , ServletException{


		request.getRequestDispatcher("messages.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request , HttpServletResponse response)
				throws IOException , ServletException{
		List<String> messages = new ArrayList<String>();

		if(isValid(request , messages) == true){
			Message message = getMessage(request);
			new MessageService().register(message);

			response.sendRedirect("./");
		}else{
			Message message = getMessage(request);
			request.setAttribute("errorMessages", messages);
			request.setAttribute("message", message);

			request.getRequestDispatcher("messages.jsp").forward(request, response);

		}
	}

	private Message getMessage(HttpServletRequest request )throws IOException , ServletException{
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("loginUser");
		Message message = new Message();
		if(!StringUtils.isBlank(request.getParameter("subject"))){
			message.setSubject(request.getParameter("subject"));
		}
		if(!StringUtils.isBlank(request.getParameter("text"))){
			message.setText(request.getParameter("text"));
		}
		if(!StringUtils.isBlank(request.getParameter("category"))){
			message.setCategory(request.getParameter("category"));
		}
		message.setUserId(user.getId());
		return message;
	}

	private boolean isValid(HttpServletRequest request , List<String> messages){
		String subject = request.getParameter("subject");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if(StringUtils.isBlank(subject)){
			messages.add("件名を入力してください");
			request.setAttribute("errorMessages", messages);

		}
		if(subject.length() > 30){
			messages.add("件名は30文字以内で入力してください");
			request.setAttribute("errorMessages", messages);

		}
		if(StringUtils.isBlank(text)){
			messages.add("本文を入力してください");
			request.setAttribute("errorMessages", messages);

		}
		if(text.length() > 1000){
			messages.add("本文は1000文字以内で入力してください");
			request.setAttribute("errorMessages", messages);

		}
		if(StringUtils.isBlank(category)){
			messages.add("カテゴリを入力してください");
			request.setAttribute("errorMessages", messages);

		}
		if(category.length() > 10){
			messages.add("カテゴリは10文字以内で入力してください");
			request.setAttribute("errorMessages", messages);

		}
		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
