package takizawa_yuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import takizawa_yuki.beans.BranchesDepartments;
import takizawa_yuki.beans.User;
import takizawa_yuki.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
				throws IOException , ServletException{
		List<BranchesDepartments> branches = new UserService().getBranch();
		List<BranchesDepartments> departments = new UserService().getDepartment();
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);

		request.getRequestDispatcher("signup.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request , HttpServletResponse response)
				throws IOException , ServletException{
		List<String> messages = new ArrayList<String>();
		List<BranchesDepartments> branches = new UserService().getBranch();
		List<BranchesDepartments> departments = new UserService().getDepartment();
		request.setAttribute("branches", branches);
		request.setAttribute("departments", departments);


		if(isValid(request , messages) == false){
			request.setAttribute("errorMessages", messages);
			User user = getUser(request);
			request.setAttribute("signupUser", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}else{
			User user = getUser(request);
			new UserService().register(user);

			response.sendRedirect("management");
		}
	}

	private User getUser (HttpServletRequest request )throws IOException , ServletException{
		User user = new User();
		user.setLoginId(request.getParameter("loginId"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		if(request.getParameter("branchId") != ""){
			user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		}
		if(request.getParameter("departmentId") != ""){
			user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));
		}
		return user;
	}

	private boolean isValid(HttpServletRequest request , List<String> messages){
		String name = request.getParameter("name");
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");

		if(StringUtils.isBlank(name)){
			messages.add("名前を入力してください");
			request.setAttribute("errorMessages", messages);
		}
		if(name.length() > 10){
			messages.add("名前は10文字以下で入力してください");
			request.setAttribute("errorMessages", messages);

		}
		if(loginId == ""){
			messages.add("ログインIDを入力してください");
			request.setAttribute("errorMessages", messages);

		}else if(loginId.length() < 6 || loginId.length() > 20){
			messages.add("ログインIDは6文字以上20文字以下で入力してください");
			request.setAttribute("errorMessages", messages);

		}else if(!loginId.matches("^[0-9a-zA-Z]+$")){
			messages.add("ログインIDは半角英数字で入力してください");
			request.setAttribute("errorMessages", messages);

		}
		if(password == ""){
			messages.add("パスワードを入力してください");
			request.setAttribute("errorMessages", messages);

		}else if(password.length() < 6 || password.length() > 20){
			messages.add("パスワードは6文字以上20文字以下で入力してください");
			request.setAttribute("errorMessages", messages);

		}else if(!password.matches("^[0-9a-zA-Z]+$")){
			messages.add("パスワードは半角英数字で入力してください");
			request.setAttribute("errorMessages", messages);

		}else if(!password.equals(password2)){
			messages.add("パスワードが一致しません");
			request.setAttribute("errorMessages", messages);

		}
		if(new UserService().getLoginId(request.getParameter("loginId")).getLoginId() != null){
			messages.add("ログインIDが重複しています");
			request.setAttribute("errorMessages", messages);

		}
		if(request.getParameter("branchId") == ""){
			messages.add("支店を選択してください");
			request.setAttribute("errorMessages", messages);

		}
		if(request.getParameter("departmentId") == ""){
			messages.add("役職を選択してください");
			request.setAttribute("errorMessages", messages);

		}

		if(messages.size() == 0){
			return true;
		}else{
			return false;
		}
	}
}
