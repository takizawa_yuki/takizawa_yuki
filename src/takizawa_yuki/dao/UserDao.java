package takizawa_yuki.dao;
import static takizawa_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import takizawa_yuki.beans.User;
import takizawa_yuki.exception.NoRowsUpdatedRuntimeException;
import takizawa_yuki.exception.SQLRuntimeException;

public class UserDao {
	public void insert(Connection connection , User user){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users (");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(", is_deleted");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getDepartmentId());
			ps.setInt(6, 0);
			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public User getUser(Connection connection , String loginId , String password){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if(userList.isEmpty() == true ){
				return null;
			}else if(2 <= userList.size()){
				throw new IllegalStateException("2 <= userList.size()");
			}else{
				return userList.get(0);

			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs)throws SQLException{

		List<User> ret = new ArrayList<User>();

		try{
			while (rs.next()){
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int branchId = rs.getInt("branch_id");
				int departmentId = rs.getInt("department_id");
				int isDeleted = rs.getInt("is_deleted");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setDepartmentId(departmentId);
				user.setIsDeleted(isDeleted);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);

				ret.add(user);
			}
			return ret;
		}finally{
			close(rs);

		}
	}
	public List<User> getUser(Connection connection){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT");
			sql.append(" users.id as id ");
			sql.append(", users.login_id as login_id");
			sql.append(", users.name as name");
			sql.append(", branches.name as branch_name");
			sql.append(", departments.name as department_name");
			sql.append(", users.is_deleted as is_deleted ");
			sql.append(" FROM  users ");
			sql.append("INNER JOIN branches ON users.branch_id = branches.id ");
			sql.append(" INNER JOIN departments ON users.department_id = departments.id ");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUsersList(rs);
			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);

		}finally{
			close(ps);
		}

	}

	private List<User> toUsersList(ResultSet rs)throws SQLException{

		List<User> ret = new ArrayList<User>();
		try{
			while(rs.next()){
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				String branchName = rs.getString("branch_name");
				String departmentName = rs.getString("department_name");
				int isDeleted = rs.getInt("is_deleted");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setName(name);
				user.setBranchName(branchName);
				user.setDepartmentName(departmentName);
				user.setIsDeleted(isDeleted);

				ret.add(user);

			}
			return ret;
		}finally{
			close(rs);

		}
	}
	public void update (Connection connection , User user){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getDepartmentId());
			ps.setInt(6, user.getId());

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public User getUser(Connection connection , int id){

		PreparedStatement ps = null;
		try{
			String sql ="SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if(userList.isEmpty() == true){
				return null;
			}else if(2 <= userList.size()){
				throw new IllegalStateException("2 <= userList.size()");
			}else{
				return userList.get(0);
			}

		}catch(SQLException e){
			throw new SQLRuntimeException(e);

		}finally{
			close(ps);
		}
	}



	public User getLoginId(Connection connection , String loginId){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT  login_id FROM users WHERE login_id = ? ";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);

			ResultSet rs = ps.executeQuery();
			User ret = toLoginUser(rs);

			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public User toLoginUser(ResultSet rs)throws SQLException {

		try{
			User checkLoginId = new User();
			while(rs.next()){
				String loginId = rs.getString("login_id");
				checkLoginId.setLoginId(loginId);

			}
			return checkLoginId;
		}finally{
			close(rs);
		}
	}

	public void isDeleted(Connection connection , User user){

		PreparedStatement ps = null;
		try{
			String sql = "UPDATE users SET is_deleted = ? WHERE id = ? ";

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, user.getIsDeleted());
			ps.setInt(2, user.getId());

			int count = ps.executeUpdate();
			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

}
