package takizawa_yuki.dao;

import static takizawa_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import takizawa_yuki.beans.Message;
import takizawa_yuki.beans.UserMessage;
import takizawa_yuki.exception.SQLRuntimeException;

public class UserMessageDao {
	public List<UserMessage> getUserMessages(Connection connection , int num){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append(" messages.id as id ");
			sql.append(", messages.subject as subject ");
			sql.append(", messages.text as text");
			sql.append(", messages.user_id as user_id ");
			sql.append(", messages.category as category ");
			sql.append(", users.name as name ");
			sql.append(", messages.created_date as created_date ");
			sql.append(" FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());


			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toUserMessageList(rs);
			return ret;

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public List<UserMessage> toUserMessageList(ResultSet rs)throws SQLException{

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try{
			while(rs.next()){
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				int userId = rs.getInt("user_id");
				String category = rs.getString("category");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setSubject(subject);
				message.setText(text);
				message.setUserId(userId);
				message.setCategory(category);
				message.setName(name);
				message.setCreatedDate(createdDate);

				ret.add(message);

			}
			return ret;
		}finally{
			close(rs);
		}
	}

	public List<UserMessage> refine(Connection connection , int num , String category ,Date createdDateMin , Date createdDateMax){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append(" messages.id as id ");
			sql.append(", messages.subject as subject ");
			sql.append(", messages.text as text");
			sql.append(", messages.user_id as user_id ");
			sql.append(", messages.category as category");
			sql.append(", users.name as name ");
			sql.append(", messages.created_date as created_date ");
			sql.append(" FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.category  LIKE '%"+category+"%' ");
			sql.append("AND messages.created_date >= '"+createdDateMin+" 00:00:00' AND messages.created_date < '"+createdDateMax+" 23:59:59' ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserMessage> ret = toMessageList(rs);
			return ret;

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public List<UserMessage> toMessageList(ResultSet rs)throws SQLException{

		List<UserMessage> ret = new ArrayList<UserMessage>();
		try{
			while(rs.next()){
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				int userId = rs.getInt("user_id");
				String category = rs.getString("category");
				String name = rs.getString("name");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserMessage message = new UserMessage();
				message.setId(id);
				message.setSubject(subject);
				message.setText(text);
				message.setUserId(userId);
				message.setCategory(category);
				message.setName(name);
				message.setCreatedDate(createdDate);

				ret.add(message);

			}
			return ret;
		}finally{
			close(rs);
		}
	}



	public Message createdDateMin(Connection connection ){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT created_date FROM messages WHERE created_date = (select min(created_date) from messages)";

			ps = connection.prepareStatement(sql);
			Message messageCreatedDate = new Message();

			ResultSet rs = ps.executeQuery();
			try{
				while(rs.next()){
					Date createdDate = rs.getDate("created_date");
					messageCreatedDate.setCreatedDate(createdDate);
				}
			}finally{
				close(ps);
			}

			return messageCreatedDate;


		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}
	public Message createdDateMax(Connection connection ){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT created_date FROM messages WHERE created_date = (select max(created_date) from messages)";

			ps = connection.prepareStatement(sql);
			Message messageCreatedDate = new Message();

			ResultSet rs = ps.executeQuery();
			try{
				while(rs.next()){
					Date createdDate = rs.getDate("created_date");
					messageCreatedDate.setCreatedDate(createdDate);
				}
			}finally{
				close(ps);
			}

			return messageCreatedDate;


		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

}
