package takizawa_yuki.dao;

import static takizawa_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import takizawa_yuki.beans.BranchesDepartments;
import takizawa_yuki.exception.SQLRuntimeException;

public class BranchDepartmentDao {
	public List<BranchesDepartments> getBranch(Connection connection){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM branches ";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<BranchesDepartments> ret = toBranches(rs);

			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public List<BranchesDepartments> toBranches(ResultSet rs)throws SQLException {

		try{
			List<BranchesDepartments> ret = new ArrayList<BranchesDepartments>();
			while(rs.next()){
				int id = rs.getInt("id");
				String name = rs.getString("name");

				BranchesDepartments toBranches= new BranchesDepartments();
				toBranches.setId(id);
				toBranches.setBranchName(name);

				ret.add(toBranches);
			}
			return ret;
		}finally{
			close(rs);
		}
	}

	public List<BranchesDepartments> getDepartment(Connection connection){
		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM departments ";

			ps = connection.prepareStatement(sql);

			ResultSet rs = ps.executeQuery();
			List<BranchesDepartments> ret = toDepartments(rs);

			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}
	public List<BranchesDepartments> toDepartments(ResultSet rs)throws SQLException {

		try{
			List<BranchesDepartments> ret = new ArrayList<BranchesDepartments>();
			while(rs.next()){
				int id = rs.getInt("id");
				String name = rs.getString("name");

				BranchesDepartments toDepartments = new BranchesDepartments();
				toDepartments.setId(id);
				toDepartments.setDepartmentName(name);

				ret.add(toDepartments);
			}
			return ret;
		}finally{
			close(rs);
		}
	}
}
