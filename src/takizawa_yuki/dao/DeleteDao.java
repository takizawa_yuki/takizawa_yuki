package takizawa_yuki.dao;

import static takizawa_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import takizawa_yuki.exception.SQLRuntimeException;

public class DeleteDao {
	public void commentDelete(Connection connection , int id){

		PreparedStatement ps = null;
		try{
			String sql = "DELETE FROM comments WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();


		}catch(SQLException e){
			throw new SQLRuntimeException(e);

		}finally{
			close(ps);
		}
	}

	public void messageDelete(Connection connection , int id){

		PreparedStatement ps = null;
		try{
			String sql = "DELETE FROM messages WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();


		}catch(SQLException e){
			throw new SQLRuntimeException(e);

		}finally{
			close(ps);
		}
	}


}
