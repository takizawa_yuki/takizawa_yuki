package takizawa_yuki.dao;

import static takizawa_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import takizawa_yuki.beans.Comments;
import takizawa_yuki.exception.SQLRuntimeException;

public class CommentDao {
	public void insert(Connection connection , Comments comment){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments (");
			sql.append(" user_id");
			sql.append(", text ");
			sql.append(", message_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(" ) VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getUserId());
			ps.setString(2, comment.getText());
			ps.setInt(3, comment.getMessageId());
			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

}
