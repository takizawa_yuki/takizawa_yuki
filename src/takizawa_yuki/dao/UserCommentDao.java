package takizawa_yuki.dao;

import static takizawa_yuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import takizawa_yuki.beans.UserComment;
import takizawa_yuki.exception.SQLRuntimeException;

public class UserCommentDao {
	public List<UserComment> getUserComment (Connection connection , int num){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append(" comments.id as id ");
			sql.append(", comments.user_id as user_id" );
			sql.append(", comments.text as text ");
			sql.append(", users.name as name ");
			sql.append(", comments.message_id as message_id ");
			sql.append(", comments.created_date as created_date ");
			sql.append(" FROM comments ");
			sql.append(" INNER JOIN users ");
			sql.append(" ON comments.user_id = users.id ");
			sql.append(" ORDER BY created_date ASC limit " + num );

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toUserCommentList(rs);
			return ret;
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally{
			close(ps);
		}
	}

	public List<UserComment> toUserCommentList(ResultSet rs)throws SQLException{

		List<UserComment> ret = new ArrayList<UserComment>();
		try{
			while(rs.next()){
				int id = rs.getInt("id");
				int userId = rs.getInt("user_id");
				String text = rs.getString("text");
				String name = rs.getString("name");
				int messageId = rs.getInt("message_id");
				Timestamp createdDate = rs.getTimestamp("created_date");

				UserComment comment = new UserComment();
				comment.setId(id);
				comment.setUserId(userId);
				comment.setText(text);
				comment.setName(name);
				comment.setMessageId(messageId);
				comment.setCreatedDate(createdDate);

				ret.add(comment);
			}
			return ret;
		}finally{
			close(rs);
		}
	}

}
