package takizawa_yuki.service;

import static takizawa_yuki.utils.CloseableUtil.*;
import static takizawa_yuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import takizawa_yuki.beans.Comments;
import takizawa_yuki.beans.UserComment;
import takizawa_yuki.dao.CommentDao;
import takizawa_yuki.dao.UserCommentDao;

public class CommentService {

	public void register(Comments comment){

		Connection connection = null;
		try{
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection , comment);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List <UserComment> getComment(){
		Connection connection = null;
		try{
			connection = getConnection();

			UserCommentDao commentDao = new UserCommentDao();
			List<UserComment> ret = commentDao.getUserComment(connection , LIMIT_NUM);

			commit(connection);

			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;

		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);

		}
	}

}
