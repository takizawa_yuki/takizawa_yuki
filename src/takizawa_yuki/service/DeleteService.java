package takizawa_yuki.service;

import static takizawa_yuki.utils.CloseableUtil.*;
import static takizawa_yuki.utils.DBUtil.*;

import java.sql.Connection;

import takizawa_yuki.dao.DeleteDao;

public class DeleteService {
	public void commentDelete(int id){

		Connection connection = null;
		try{
			connection = getConnection();

			DeleteDao deleteDao = new DeleteDao();
			deleteDao.commentDelete(connection, id);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void messageDelete(int id){

		Connection connection = null;
		try{
			connection = getConnection();

			DeleteDao deleteDao = new DeleteDao();
			deleteDao.messageDelete(connection, id);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

}
