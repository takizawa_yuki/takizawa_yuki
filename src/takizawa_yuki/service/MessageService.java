package takizawa_yuki.service;

import static takizawa_yuki.utils.CloseableUtil.*;
import static takizawa_yuki.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

import takizawa_yuki.beans.Message;
import takizawa_yuki.beans.UserMessage;
import takizawa_yuki.dao.MessageDao;
import takizawa_yuki.dao.UserMessageDao;

public class MessageService {

	public void register(Message message){

		Connection connection = null;
		try{
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection , message);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;

		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<UserMessage> getMessage(){

		Connection connection = null;
		try{
			connection = getConnection();

			UserMessageDao messageDao = new UserMessageDao();
			List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			close(connection);
		}
	}


	public List<UserMessage> getMessage(String category , Date createdDateMin , Date createdDateMax){

		Connection connection = null;
		try{
			connection = getConnection();

			UserMessageDao messageDao = new UserMessageDao();
			List<UserMessage> ret = messageDao.refine(connection, LIMIT_NUM, category , createdDateMin , createdDateMax);

			commit(connection);

			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			close(connection);
		}
	}


	public static Message createdDateMin(){

		Connection connection = null;
		try{
			connection = getConnection();

			UserMessageDao messageDao = new UserMessageDao();
			Message ret = messageDao.createdDateMin(connection );

			commit(connection);

			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			close(connection);
		}
	}
	public static Message createdDateMax(){

		Connection connection = null;
		try{
			connection = getConnection();

			UserMessageDao messageDao = new UserMessageDao();
			Message ret = messageDao.createdDateMax(connection );

			commit(connection);

			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			close(connection);
		}
	}


}
