package takizawa_yuki.service;

import static takizawa_yuki.utils.CloseableUtil.*;
import static takizawa_yuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import takizawa_yuki.beans.BranchesDepartments;
import takizawa_yuki.beans.User;
import takizawa_yuki.dao.BranchDepartmentDao;
import takizawa_yuki.dao.UserDao;
import takizawa_yuki.utils.CipherUtil;

public class UserService {

	public void register(User user){

		Connection connection = null;
		try{
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection , user);

			commit(connection);
		}catch(RuntimeException e){
			rollback(connection);
			throw e;

		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public List<User> getUser(){

		Connection connection = null;
		try{
			connection = getConnection();

			UserDao UserDao = new UserDao();
			List<User> ret = UserDao.getUser(connection);

			commit(connection);

			return ret;
		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}
	}

	public void update(User user){

		Connection connection = null;
		try{
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			close(connection);
		}
	}
	public void emptyPassUpdate(User user){

		Connection connection = null;
		try{
			connection = getConnection();


			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			close(connection);
		}
	}


	public User getUser(int id){

		Connection connection = null;
		try{
			connection = getConnection();
			UserDao userDao = new UserDao();
			User user = userDao.getUser(connection , id);
			commit(connection);

			return user;

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}

	}

	public List<BranchesDepartments> getDepartment(){

		Connection connection = null;
		try{
			connection = getConnection();
			BranchDepartmentDao BranchDepartmentDao = new BranchDepartmentDao();
			List<BranchesDepartments> departmentName = BranchDepartmentDao.getDepartment(connection);
			commit(connection);

			return departmentName;

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}

	}

	public List<BranchesDepartments> getBranch(){

		Connection connection = null;
		try{
			connection = getConnection();
			BranchDepartmentDao BranchDepartmentDao = new BranchDepartmentDao();
			List<BranchesDepartments> branchName = BranchDepartmentDao.getBranch(connection);
			commit(connection);

			return branchName;

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;
		}finally{
			close(connection);
		}

	}

	public void isDeleted(User user){

		Connection connection = null;
		try{
			connection = getConnection();

			UserDao userDao = new UserDao();
			userDao.isDeleted(connection, user);

			commit(connection);

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			close(connection);
		}
	}
	public User getLoginId(String loginId){

		Connection connection = null;
		try{
			connection = getConnection();

			UserDao userDao = new UserDao();
			User checkLoginId = userDao.getLoginId(connection , loginId);

			commit(connection);

			return checkLoginId;

		}catch(RuntimeException e){
			rollback(connection);
			throw e;
		}catch(Error e){
			rollback(connection);
			throw e;

		}finally{
			close(connection);
		}
	}
}
