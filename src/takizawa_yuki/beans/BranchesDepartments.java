package takizawa_yuki.beans;

import java.io.Serializable;

public class BranchesDepartments implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
	private String branchName;
	private String departmentName;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}


}
